-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: frogstead
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `frog`
--

DROP TABLE IF EXISTS `frog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frog` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique number id for frog',
  `name` varchar(255) NOT NULL,
  `species` varchar(50) NOT NULL,
  `birth_date` date NOT NULL,
  `is_death` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - alive, 1 - death',
  `death_date` date DEFAULT NULL,
  `gender` enum('male','female') NOT NULL,
  `is_deleted` enum('0','1') DEFAULT '0' COMMENT '0 - keep, 1 - deleted',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frog`
--

LOCK TABLES `frog` WRITE;
/*!40000 ALTER TABLE `frog` DISABLE KEYS */;
INSERT INTO `frog` VALUES (1,'Ardie','darwin','2016-01-15','0',NULL,'male','0','2017-04-15 04:33:31','2017-04-16 21:18:47'),(2,'Artie','darwin','2016-02-16','0',NULL,'female','0','2017-04-15 05:27:02','2017-04-15 05:27:02'),(3,'Bazoo','goliath','2016-03-10','0',NULL,'male','0','2017-04-15 05:27:25','2017-04-16 16:44:32'),(4,'Bully','goliath','2016-03-30','0',NULL,'female','1','2017-04-15 05:27:58','2017-04-17 06:09:23'),(5,'Croak','northern leopard','2016-04-06','0',NULL,'male','0','2017-04-15 05:29:05','2017-04-15 05:29:05'),(6,'Carmine','northern leopard','2016-04-20','0',NULL,'female','0','2017-04-15 05:29:30','2017-04-15 05:29:30'),(7,'Diego','ornate horned','2016-05-12','0',NULL,'male','0','2017-04-15 05:30:10','2017-04-15 05:30:10'),(8,'Dixie','ornate horned','2016-05-26','0',NULL,'female','0','2017-04-15 05:30:28','2017-04-15 05:30:28'),(9,'Ethan','poison dart','2016-06-10','0',NULL,'male','0','2017-04-15 05:30:53','2017-04-15 05:30:53'),(10,'Edwinna','poison dart','2016-06-29','0',NULL,'female','0','2017-04-15 05:31:08','2017-04-15 05:31:08'),(11,'Fletcher','wood','2016-07-05','0',NULL,'male','0','2017-04-15 05:31:41','2017-04-15 05:31:41'),(12,'Flippy','wood','2016-07-23','0',NULL,'female','0','2017-04-15 05:32:01','2017-04-15 05:32:01');
/*!40000 ALTER TABLE `frog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frog_mating`
--

DROP TABLE IF EXISTS `frog_mating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frog_mating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `male_frog_id` int(11) NOT NULL,
  `female_frog_id` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - init, 1 - inprocess, 2 - complete',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - keep, 1 - deleted',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frog_mating`
--

LOCK TABLES `frog_mating` WRITE;
/*!40000 ALTER TABLE `frog_mating` DISABLE KEYS */;
INSERT INTO `frog_mating` VALUES (1,1,2,'2','0','2017-04-15 07:46:51','2017-04-17 09:24:29'),(2,3,4,'2','1','2017-04-17 02:57:40','2017-04-17 06:09:37'),(3,1,8,'0','0','2017-04-17 03:44:41','2017-04-17 06:06:48'),(4,7,2,'0','0','2017-04-17 06:09:55','2017-04-17 06:09:55');
/*!40000 ALTER TABLE `frog_mating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - inactive, 1 - active',
  `is_admin` enum('0','1') DEFAULT '0' COMMENT '0 - user, 1 - admin',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - keep, 1 - deleted',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','User','admin@admin.com','$2y$10$EMRYsx7h8VznYIkNpEgfvehDLwCUIJrPGZM5xoXQvhYWceHILxVnK','EwAFeq1qEm5QCL7iWfGV1AT68hVp4YZ4qnT6cYAxg65nIs6w4xH9rqfktjmD','1','1','0','2017-04-14 06:16:06','2017-04-18 06:41:32'),(2,'Ruturaj','Maniyar','ruturaj.maniyar@gmail.com','$2y$10$R9rO.NzkXKtkowIuRFxv/unVrzcrl4Vf7nSNaVULBjOU.t6gCA0Py','Le7DkgiErqeMBzaGINRI6ZBxEHlVm23C4IKskHzEPNI1k6vtmnxQrTsxyzxg','1','0','0','2017-04-14 06:17:39','2017-04-18 06:34:56'),(3,'Allen','Joen','allen.joen@test.com','$2y$10$Iz5oHw1uIzc/5HnHD69kxuTfXwRsWMOL03nNkh5LW55h3PMAPpDe2','ESzMzGGPqmh6EFlup4RuXYZWYyf9RUdnjqaKZzAcP5iecv7ZNqYm6K8R6ASk','1','0','0','2017-04-16 10:44:13','2017-04-16 16:16:09'),(4,'kartik','konak','kartik.konak@test.com','$2y$10$NhPYJ0KfzKKfx/0lW3qJBOHwuEDLVYnCppJ0Ricagm5TudKJatKFa','MJ4jWyS77fzwGepW65d7jy9w0BHTqbAZ0MgnO5fTgFsVCiJXAPeRyDks24vs','1','0','0','2017-04-16 10:46:48','2017-04-17 11:40:40');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-18 13:36:52
