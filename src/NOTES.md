<!---
Add your comments / notes and thoughts to this doc

Any special instructions to running your code?
-->

** Build this application with

 - PHP 5.6
 - Laravel 5.4
 - Apache 2.4
 - MySql Server 5.7.17
 - Composer 1.3.0
 - Ubuntu 16.04
 
 # Installation
 
 ** Step 1 :
  
  - checkout from "https://ruturajmaniyar@bitbucket.org/ruturajmaniyar/tech_test_blank.git"
 
 ** Step 2 :
 
  - Goto '/src' folder and install composer using below command
  		
		$ composer install
  
  - Then copy '.env.example' file as '.env' in same location
  	
		$ sudo cp .env.example .env
		
 ** Step 3 :
 
  - Give permission to project folder using below command
  	
		$ sudo chmod -Rf 777 {project folder name}
		
 ** Step 4 :
 
  - Goto '/src' folder and generate laravel key using following command
  		
		$ php artisan key:generate
		
# Database Setup

** Note 1 that you want to install mysql if not available in your system

** Note 2 mysqldump also available with this project root location name as 'frogstead.sql'

** Import mysql 

	- Create Database name as 'frogstead' with below query
	
		SELECT DATABASE `frogstead`;
	
	- Import mysql dump file into frogstead database

		$ mysql -u {username} -p frogstead < {path}/frogstead.sql
		
** Laravel Database configuration

	- Open '/src/.env' file and set code somethings like below
	
		DB_CONNECTION=mysql
		DB_HOST=127.0.0.1
		DB_PORT=3306
		DB_DATABASE=frogstead
		DB_USERNAME={username}
		DB_PASSWORD={password}
		
		than save file
		
 # Run Project
 
 	Run following command before run project
	
		$ php artisan cache:clear
		
		$ php artisan route:clear
		
		$ php artisan optimize
		
	Now Run project using below command
	
		$ php artisan serve
		
	After run above command, your terminal return URL of applicaion, Copy this url and run with browser.
	
 # User Management
 
 	There are two type of User One is ADMIN and second is Simple User.
	
	Database contain Admin user entry by default.
	
	Admin User Credential 
		- email_id : admin@admin.com
		- password : thinker99
	
	Simple User Creadential
		- email_id : ruturaj.maniyar@gmail.com
		- password : 123456
		
** Note that Application can not create any admin user. If you want to create simple user, use register functionality. When create user using register functionality, Created user role as 'Simple User'.

** Note that delete functionality right only have admin user, Simple User can not do that.

  