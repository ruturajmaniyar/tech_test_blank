/**
 * Created by flinnt-php-5 on 15/4/17.
 */
$('document').ready(function () {

    var deathStatus = ($('#is_death').val());

    if(deathStatus == "0"){
        $('#death_date').val("").attr("disabled", true);
        $('#name').attr("disabled", false);
        $('#species').attr("disabled", false);
        $('#birth_date').attr("disabled", false);
        $('#gender').attr("disabled", false);
    } else {
        $('#death_date').attr("disabled", false);
        $('#name').attr("disabled", true);
        $('#species').attr("disabled", true);
        $('#birth_date').attr("disabled", true);
        $('#gender').attr("disabled", true);
    }
});

$('#is_death').change(function () {
    var deathStatus = ($('#is_death').val());

    if(deathStatus == "0"){
        $('#death_date').val("").attr("disabled", true);
        $('#name').attr("disabled", false);
        $('#species').attr("disabled", false);
        $('#birth_date').attr("disabled", false);
        $('#gender').attr("disabled", false);
    } else {
        $('#death_date').attr("disabled", false);
        $('#name').attr("disabled", true);
        $('#species').attr("disabled", true);
        $('#birth_date').attr("disabled", true);
        $('#gender').attr("disabled", true);
    }
});
