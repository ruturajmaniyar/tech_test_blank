<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'HomeController@index');
});

Route::group(['prefix' => 'user'],  function () {
    Route::match(['get', 'head'], '/', ['as' => 'user.index', 'uses' => 'UserController@index']);
    Route::match(['get', 'head'], '/create', ['as' => 'user.create', 'uses' => 'UserController@create']);
    Route::post('/', ['as' => 'user.store', 'uses' => 'UserController@store']);
    Route::match(['get', 'head'], '/{id}/edit', ['as' => 'user.edit', 'uses' => 'UserController@edit']);
    Route::match(['put', 'patch'], '/{id}', ['as' => 'user.update', 'uses' => 'UserController@update']);
    Route::get('delete/{id}',array('uses' => 'UserController@destroy', 'as' => 'user.destroy'));
});

Route::group(['prefix' => 'frog'],  function () {
    Route::match(['get', 'head'], '/', ['as' => 'frog.index', 'uses' => 'FrogController@index']);
    Route::match(['get', 'head'], '/create', ['as' => 'frog.create', 'uses' => 'FrogController@create']);
    Route::post('/', ['as' => 'frog.store', 'uses' => 'FrogController@store']);
    Route::match(['get', 'head'], '/{id}/edit', ['as' => 'frog.edit', 'uses' => 'FrogController@edit']);
    Route::match(['put', 'patch'], '/{id}', ['as' => 'frog.update', 'uses' => 'FrogController@update']);
    Route::get('delete/{id}',array('as' => 'frog.destroy', 'uses' => 'FrogController@destroy'));
});

Route::group(['prefix' => 'mating'],  function () {
    Route::match(['get', 'head'], '/', ['as' => 'mating.index', 'uses' => 'FrogMatingController@index']);
    Route::match(['get', 'head'], '/create', ['as' => 'mating.create', 'uses' => 'FrogMatingController@create']);
    Route::post('/', ['as' => 'mating.store', 'uses' => 'FrogMatingController@store']);
    Route::match(['get', 'head'], '/{id}/edit', ['as' => 'mating.edit', 'uses' => 'FrogMatingController@edit']);
    Route::match(['put', 'patch'], '/{id}', ['as' => 'mating.update', 'uses' => 'FrogMatingController@update']);
    Route::get('delete/{id}',array('as' => 'mating.destroy', 'uses' => 'FrogMatingController@destroy'));
});