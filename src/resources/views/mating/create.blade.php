@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add New Frog Mating</div>
                    <div class="panel-body">

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('mating.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('male_frog_id') ? ' has-error' : '' }}">
                                <label for="male_frog_id" class="col-md-4 control-label">Male Frog</label>

                                <div class="col-md-6">
                                    <select class="form-control" id="male_frog_id", name="male_frog_id" required>
                                        <option value="">Select Male Frog</option>
                                        @foreach($maleFrogCollection as $maleFrog)
                                            <option value="{{ $maleFrog->id }}" {{ old('male_frog_id') == $maleFrog->id ? "selected" : "" }}>{{ $maleFrog->name }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('male_frog_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('male_frog_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('male_frog_id') ? ' has-error' : '' }}">
                                <label for="female_frog_id" class="col-md-4 control-label">Female Frog</label>

                                <div class="col-md-6">
                                    <select class="form-control" id="female_frog_id", name="female_frog_id" required>
                                        <option value="">Select Female Frog</option>
                                        @foreach($femaleFrogCollection as $femaleFrog)
                                            <option value="{{ $femaleFrog->id }}" {{ old('male_frog_id') == $femaleFrog->id ? "selected" : "" }}>{{ $femaleFrog->name }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('female_frog_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('female_frog_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">Status</label>

                                <div class="col-md-6" >
                                    <select class="form-control" id="status" name="status" required>
                                        <option value="0" {{ old('status') == '0' ? "selected=selected" : "" }}>Initialization</option>
                                        <option value="1" {{ old('status') == '1' ? "selected=selected" : "" }}>In Processing</option>
                                        <option value="2" {{ old('status') == '2' ? "selected=selected" : "" }}>Completed</option>
                                    </select>

                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Set Frog Mating</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
