@extends('layouts.app')

@section('content')
<p>
    <a href="{{ route('mating.create') }}" class="btn btn-primary">Set Frog Mating</a>
</p>

@include('layouts.notifications')


<div class="panel panel-default">
    <div class="panel-heading">Mating Information</div>
    <div class="panel-body">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Male Frog</th>
                <th>Female Frog</th>
                <th>Status</th>
                <th>Last Updated</th>
            </tr>
            </thead>
            <tbody>
            @forelse($frogMatings as $frogMating)
            <tr>
                <th scope="row">{{ $frogMating->id }}</th>
                <td>{{ $frogMating->maleFrog->name }}</td>
                <td>{{ $frogMating->femaleFrog->name }}</td>
                <td>
                    @if($frogMating->status == '0')
                        {{ "Initialization" }}
                    @elseif($frogMating->status == '1')
                        {{ "In Processing" }}
                    @else
                        {{ "Completed" }}
                    @endif
                </td>
                <td>{{ $frogMating->updated_at }}</td>
                <td>
                    @if($frogMating->status !== '2')
                        <a href="{{ route('mating.edit',['id' => $frogMating->id]) }}" class="btn btn-default">Change Status</a>
                    @endif
                    @if (Auth::user()->is_admin && $frogMating->status == '2')
                        <a onclick="return confirm('Really? Do you want do this!')" href="{{ route('mating.destroy',['id' => $frogMating->id]) }}" class="btn btn-danger">Delete</a>
                    @endif
                </td>
            </tr>
            @empty
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                I am sorry, But No Frog Mating available.
            </div>
            @endforelse
            </tbody>
        </table>
        {!! $frogMatings->render() !!}
    </div>
</div>
@endsection