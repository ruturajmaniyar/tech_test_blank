@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Updating a Frog</div>
                <div class="panel-body">

                    <form class="form-horizontal" role="form" action="{{ route('mating.update', ['id' => $frogMatting->id]) }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="male_frog_id" class="col-md-4 control-label">Male Frog</label>
                            <div class="col-md-6">
                                <select class="form-control" name="male_frog_id" id="male_frog_id" class="form-control" readonly>
                                    <option value="{{ $frogMatting->male_frog_id }}" >{{ $frogMatting->maleFrog->name }}</option>
                                </select>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('male_frog_id') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="female_frog_id" class="col-md-4 control-label">Female Frog</label>
                            <div class="col-md-6">
                                <select class="form-control" name="female_frog_id" id="female_frog_id" class="form-control" readonly>
                                    <option value="{{ $frogMatting->female_frog_id }}" >{{ $frogMatting->femaleFrog->name }}</option>
                                </select>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('female_frog_id') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Status</label>
                            <div class="col-md-6">
                                <select class="form-control" name="status" id="status" class="form-control" required>
                                    <option value="0" {{ $frogMatting->status == "0" ? "selected=selected" : "" }}>Initialization</option>
                                    <option value="1" {{ $frogMatting->status == "1" ? "selected=selected" : "" }}>In Processing</option>
                                    <option value="2" {{ $frogMatting->status == "2" ? "selected=selected" : "" }}>Completed</option>
                                </select>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('status') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Update Status</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageScript')
    <script type="text/javascript" src="{{ asset('js/frog/frog.js') }}"></script>
@endsection