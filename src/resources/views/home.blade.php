@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total Users</span>
                    <span class="info-box-number">{{ $userCount }}</span>
                </div>
                <div class="info-box-content">
                    <span class="info-box-text">Active Users</span>
                    <span class="info-box-number">{{ $activeUserCount }}</span>
                </div>
                <div class="info-box-content">
                    <span class="info-box-text">Inactive Users</span>
                    <span class="info-box-number">{{ $inactiveUserCount }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-foursquare"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total Fogs</span>
                    <span class="info-box-number">{{ $frogCount }}</span>
                </div>
                <div class="info-box-content">
                    <span class="info-box-text">Male Fogs</span>
                    <span class="info-box-number">{{ $maleFrogCount }}</span>
                </div>
                <div class="info-box-content">
                    <span class="info-box-text">Female Fogs</span>
                    <span class="info-box-number">{{ $femaleFrogCount }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-venus-double"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Frog Mating</span>
                    <span class="info-box-number">{{ $matingCount }}</span>
                </div>

                <div class="info-box-content">
                    <span class="info-box-text">In Process Mating</span>
                    <span class="info-box-number">{{ $processingMatingCount }}</span>
                </div>

                <div class="info-box-content">
                    <span class="info-box-text">Completed Mating</span>
                    <span class="info-box-number">{{ $completedMatingCount }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>
@endsection
