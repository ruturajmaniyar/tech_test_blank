@extends('layouts.app')

@section('content')

    @include('layouts.notifications')

    <div class="panel panel-default">
        <div class="panel-heading">User</div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email Id</th>
                    <th>Status</th>
                    <th>Role</th>
                    <th>Last Updated</th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->email_id }}</td>
                    <td>{{ $user->status ? "Active" : "Inactive" }}</td>
                    <td>{{ $user->is_admin ? "Super Admin" : "User" }}</td>
                    <td>{{ $user->updated_at }}</td>
                    <td>
                        @if (Auth::user()->is_admin && Auth::id() != $user->id)
                            <a href="{{ route('user.edit',['id' => $user->id]) }}" class="btn btn-default">Edit</a>
                            {{ method_field('DELETE') }}
                            <a onclick="return confirm('Really? Do you want do this!')" href="{{ route('user.destroy',['id' => $user->id]) }} " class="btn btn-danger">Delete</a>
                        @else
                            @if (Auth::id() == $user->id)
                                <a href="{{ route('user.edit',['id' => $user->id]) }}" class="btn btn-default">Edit</a>
                            @endif
                        @endif
                    </td>
                </tr>
                @empty
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    I am sorry, But No user available.
                </div>
                @endforelse
                </tbody>
            </table>
            {!! $users->render() !!}
        </div>
    </div>
@endsection