@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Updating a User</div>
                <div class="panel-body">

                    <form class="form-horizontal" role="form" action="{{ route('user.update', ['id' => $user->id]) }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="first_name" class="col-md-4 control-label">First Name</label>
                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $user->first_name }}" required autofocus>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('first_name') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Last Name</label>
                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $user->last_name }}" required>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('last_name') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email_id" class="col-md-4 control-label">Email Id</label>
                            <div class="col-md-6">
                                <input id="email_id" type="text" class="form-control" name="email_id" value="{{ $user->email_id }}" disabled>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('email_id') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Status</label>
                            <div class="col-md-6">
                                <select class="form-control" id="status" name="status" >
                                    <option value="0" {{ $user->status == "0" ? "selected=selected" : "" }}>Inactive</option>
                                    <option value="1" {{ $user->status == "1" ? "selected=selected" : "" }}>Active</option>
                                </select>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('status') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Update this User</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection