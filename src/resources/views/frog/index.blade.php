@extends('layouts.app')

@section('content')
<p>
    <a href="{{ route('frog.create') }}" class="btn btn-primary">Add New Frog</a>
</p>

@include('layouts.notifications')


<div class="panel panel-default">
    <div class="panel-heading">Frog</div>
    <div class="panel-body">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Species</th>
                <th>Birth Date</th>
                <th>Gender</th>
                <th>Status</th>
                <th>Death Date</th>
                <th>Last Updated</th>
            </tr>
            </thead>
            <tbody>
            @forelse($frogs as $frog)
            <tr>
                <th scope="row">{{ $frog->id }}</th>
                <td>{{ $frog->name }}</td>
                <td>{{ $frog->species }}</td>
                <td>{{ $frog->birth_date }}</td>
                <td>{{ $frog->gender }}</td>
                <td>{{ $frog->is_death ? "Death" : "Alive" }}</td>
                <td>{{ $frog->death_date==null ? "--" : $frog->death_date  }}</td>
                <td>{{ $frog->updated_at }}</td>
                <td>
                    <a href="{{ route('frog.edit',['id' => $frog->id]) }}" class="btn btn-default">Edit</a>
                    @if (Auth::user()->is_admin)
                        <a onclick="return confirm('Really? Do you want do this!')" href="{{ route('frog.destroy',['id' => $frog->id]) }}" class="btn btn-danger">Delete</a>
                    @endif
                </td>
            </tr>
            @empty
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                I am sorry, But No Frog available.
            </div>
            @endforelse
            </tbody>
        </table>
        {!! $frogs->render() !!}
    </div>
</div>
@endsection