@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add New Frog</div>
                    <div class="panel-body">

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('frog.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Frog Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('species') ? ' has-error' : '' }}">
                                <label for="species" class="col-md-4 control-label">Species</label>

                                <div class="col-md-6">
                                    <select class="form-control" id="species" name="species" required>
                                        <option value="">Select Species</option>
                                        <option value="darwin" {{ old('species') == 'darwin' ? "selected=selected" : "" }}>Darwin</option>
                                        <option value="goliath" {{ old('species') == 'goliath' ? "selected=selected" : "" }}>Goliath</option>
                                        <option value="northern leopard" {{ old('species') == 'northern leopard' ? "selected=selected" : "" }}>Northern Leopard</option>
                                        <option value="ornate horned" {{ old('species') == 'ornate horned' ? "selected=selected" : "" }}>Ornate Horned</option>
                                        <option value="poison dart" {{ old('species') == 'poison dart' ? "selected=selected" : "" }}>Poison Dart</option>
                                        <option value="wood" {{ old('species') == 'wood' ? "selected=selected" : "" }}>Wood</option>
                                    </select>

                                    @if ($errors->has('species'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('species') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                                <label for="birth_date" class="col-md-4 control-label">Birth Date</label>

                                <div class="col-md-6">
                                    <input id="birth_date" type="text" class="date form-control" name="birth_date" value="{{ old('birth_date') }}" required>

                                    @if ($errors->has('birth_date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('birth_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label for="gender" class="col-md-4 control-label">Gender</label>

                                <div class="col-md-6" >
                                    <select class="form-control" id="gender" name="gender" required>
                                        <option value="">Select Gender</option>
                                        <option value="male" {{ old('gender') == 'male' ? "selected=selected" : "" }}>Male</option>
                                        <option value="female" {{ old('gender') == 'female' ? "selected=selected" : "" }}>Female</option>
                                    </select>

                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Welcome Frog</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
