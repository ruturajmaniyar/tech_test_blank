@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Updating a Frog</div>
                <div class="panel-body">

                    <form class="form-horizontal" role="form" action="{{ route('frog.update', ['id' => $frog->id]) }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Frog Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $frog->name }}" required autofocus>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('name') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="species" class="col-md-4 control-label">Species</label>
                            <div class="col-md-6">
                                <select class="form-control" name="species" id="species" required>
                                    <option value="darwin" {{ $frog->species == 'darwin' ? "selected=selected" : "" }}>Darwin</option>
                                    <option value="goliath" {{ $frog->species == 'goliath' ? "selected=selected" : "" }}>Goliath</option>
                                    <option value="northern leopard" {{ $frog->species == 'northern leopard' ? "selected=selected" : "" }}>Northern Leopard</option>
                                    <option value="ornate horned" {{ $frog->species == 'ornate horned' ? "selected=selected" : "" }}>Ornate Horned</option>
                                    <option value="poison dart" {{ $frog->species == 'poison dart' ? "selected=selected" : "" }}>Poison Dart</option>
                                    <option value="wood" {{ $frog->species == 'wood' ? "selected=selected" : "" }}>Wood</option>
                                </select>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('species') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="birth_date" class="col-md-4 control-label">Birth Date</label>
                            <div class="col-md-6">
                                <input id="birth_date" type="text" class="date form-control" name="birth_date" value="{{ $frog->birth_date }}" required>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('birth_date') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gender" class="col-md-4 control-label">Gender</label>
                            <div class="col-md-6">
                                <select class="form-control" name="gender" id="gender" class="form-control" required>
                                    <option value="male" {{ $frog->gender == "male" ? "selected=selected" : "" }}>Male</option>
                                    <option value="female" {{ $frog->gender == "female" ? "selected=selected" : "" }}>Female</option>
                                </select>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('gender') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="is_death" class="col-md-4 control-label">Frog Status</label>
                            <div class="col-md-6">
                                <select class="form-control" name="is_death" id="is_death" class="form-control" required>
                                    <option value="0" {{ $frog->is_death == "0" ? "selected=selected" : "" }}>Alive</option>
                                    <option value="1" {{ $frog->is_death == "1" ? "selected=selected" : "" }}>Death</option>
                                </select>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('is_death') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="death_date" class="col-md-4 control-label">Death Date</label>
                            <div class="col-md-6">
                                <input id="death_date" type="text" class="date form-control" name="death_date" value="{{ $frog->death_date }}" required>
                                <span class="help-block"><p class="text-danger">{{ $errors->first('death_date') }}</p></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Update this Frog</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageScript')
    <script type="text/javascript" src="{{ asset('js/frog/frog.js') }}"></script>
@endsection