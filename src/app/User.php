<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    // use Authenticatable Trait
    use \Illuminate\Auth\Authenticatable;

    // set User Table
    protected $table = "users";

    // set fillable attributes
    protected $fillable = ['first_name', 'last_name', 'email_id', 'password', 'status', 'created_at', 'updated_at'];
}
