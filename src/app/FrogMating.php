<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrogMating extends Model
{
    // set Frog Mating table
    protected $table = "frog_mating";

    // set fillable attributes
    protected $fillable = ['male_frog_id', 'female_frog_id', 'status', 'created_at', 'updated_at'];

    public function maleFrog() {
        return $this->belongsTo('App\Frog', 'male_frog_id', 'id');
    }

    public function femaleFrog() {
        return $this->belongsTo('App\Frog', 'female_frog_id', 'id');
    }
}
