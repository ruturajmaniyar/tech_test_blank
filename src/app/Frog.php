<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frog extends Model
{
    // set Frog Table
    protected $table ='frog';

    // set fillable attributes
    protected $fillable = ['name', 'species', 'birth_date', 'is_death', 'death_date', 'gender', 'created_at', 'updated_at'];

    public function maleFrogMating(){
        return $this->hasMany('App\FrogMating', 'male_frog_id', 'id')->where('status', '2');
    }

    public function femaleFrogMating(){
        return $this->hasMany('App\FrogMating', 'female_frog_id', 'id');
    }
}
