<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FrogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|min:4|max:255',
            'species' => 'sometimes',
            'birth_date' => 'sometimes|date_format:Y-m-d',
            'gender' => 'sometimes',
            'is_death' => 'sometimes|boolean',
            'death_date' => 'sometimes|date_format:Y-m-d',
        ];
    }
}
