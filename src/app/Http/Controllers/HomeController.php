<?php

namespace App\Http\Controllers;

use App\Frog;
use App\FrogMating;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // User Data Collection
        $userCount          =   User::where('is_admin', '0')->where('is_deleted', '0')->count();
        $activeUserCount    =   User::where('is_admin', '0')->where('status', '1')->where('is_deleted', '0')->count();
        $inactiveUserCount  =   User::where('is_admin', '0')->where('status', '0')->where('is_deleted', '0')->count();

        // Frog Data Collection
        $frogCount          =   Frog::where('is_deleted', '0')->count();
        $maleFrogCount      =   Frog::where('gender', 'male')->where('is_deleted', '0')->count();
        $femaleFrogCount    =   Frog::where('gender', 'female')->where('is_deleted', '0')->count();

        // Frog Mating Data Collection
        $matingCount            =   FrogMating::where('is_deleted', '0')->count();
        $initMatingCount        =   FrogMating::where('is_deleted', '0')->where('status', '0')->count();
        $processingMatingCount  =   FrogMating::where('is_deleted', '0')->where('status', '1')->count();
        $completedMatingCount   =   FrogMating::where('is_deleted', '0')->where('status', '2')->count();

        return view('home', compact(
            'userCount', 'activeUserCount', 'inactiveUserCount', 'frogCount', 'maleFrogCount', 'femaleFrogCount', 'matingCount', 'processingMatingCount', 'completedMatingCount'));
    }
}
