<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Validate the user login request.
     *
     * Set custom message when user login with inactive
     *
     * Overwrite this method from AuthenticatesUsers trait
     *
     * @param Request|\Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|exists:users,' .$this->username() . ',status,1|exists:users,' .$this->username() . ',is_deleted,0',
            'password' => 'required',
        ], [$this->username() . '.exists' => 'User is inactive or deleted, Please contact to Administrator.']);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * Not allow to login if User is inactive
     *
     * Overwrite this method from AuthenticatesUsers trait
     *
     * @param Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        $credentials = array_add($credentials, "status", "1");
        $credentials = array_add($credentials, "is_deleted", "0");
        return $credentials;
    }

    /**
     * Get the login username to be used by the controller.
     *
     * set username as per our database field
     *
     * Overwrite this method from AuthenticatesUsers trait
     *
     * @return string
     */
    public function username()
    {
        return 'email_id';
    }


    /**
     * Logout user action
     *
     * Overwrite this method from AuthenticatesUsers trait
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/login');
    }


}
