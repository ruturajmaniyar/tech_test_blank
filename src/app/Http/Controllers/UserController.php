<?php

namespace App\Http\Controllers;

use App\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    /**
     * create new controller Instance
     *
     * check user is authorize or not
     *
     * UserController constructor.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // get all User collection
        $users = User::where(['is_admin' => '0', 'is_deleted' => "0"])->orderBy('id','DESC')->paginate(5);
        return view('user.index',compact('users'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param user $user
     */
    public function edit($id)
    {
        // check Current User is admin or logged user.
        if (Auth::user()->is_admin || Auth::id() == $id) {
            $user = User::find($id);
            return view('user.edit',compact('user'));
        } else {
            Session::flash('alert-danger', 'User not allow to perform this operation.');
            return redirect()->route('user.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param user $user
     */
    public function update(Request $request, $id)
    {
        // check Current User is admin or logged user.
        if (Auth::user()->is_admin || Auth::id() == $id) {
            $this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
            ]);

            User::find($id)->update($request->all());
            Session::flash('alert-success', 'User updated successfully.');
            return redirect()->route('user.index');
        } else {
            Session::flash('alert-danger', 'User not allow to perform this operation.');
            return redirect()->route('user.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param user $user
     */
    public function destroy($id)
    {
        // check Current User is admin or logged user.
        if (Auth::user()->is_admin) {

            // user soft delete operation perform
            $user = User::find($id);
            $user->is_deleted = "1";
            $user->save();

            Session::flash('alert-success', 'User deleted successfully.');
            return redirect()->route('user.index');
        } else {
            Session::flash('alert-danger', 'User not allow to perform this operation.');
            return redirect()->route('user.index');
        }
    }
}
