<?php

namespace App\Http\Controllers;

use App\Frog;
use App\FrogMating;
use App\Http\Requests\FrogMatingRequest;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class FrogMatingController extends Controller
{
    /**
     * create new controller Instance
     *
     * check user is authorize or not
     *
     * FrogMatingController constructor.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param FrogMatingRequest|Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // get Collection of Frog Mating information

        $frogMatings = FrogMating::where('is_deleted', '0')->orderBy('id', 'ASC')->paginate(5);
        return view('mating.index', compact('frogMatings'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        /**
         * Keep this comment
         *
         * For write sql query
         *
         * \DB::enableQueryLog();
         * dd(\DB::getQueryLog());
         *
         */

        $maleFrogCollection = Frog::select()
            ->where('gender', 'male')
            ->where('is_death', '0')
            ->where('is_deleted', '0')
            ->whereNotIn('id', function ($query)
            {
                $query->select(DB::raw("male_frog_id"))->from("frog_mating")->whereRaw("frog_mating.status != '2' " );
            })->get();

        $femaleFrogCollection = Frog::select()
            ->where('gender', 'female')
            ->where('is_death', '0')
            ->where('is_deleted', '0')
            ->whereNotIn('id', function ($query)
            {
                $query->select(DB::raw("female_frog_id"))->from("frog_mating")->whereRaw("frog_mating.status != '2' " );
            })->get();

        // render create mating form
        return view('mating.create', compact('maleFrogCollection', 'femaleFrogCollection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FrogMatingRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FrogMatingRequest $request)
    {
        // Add new Frog Mating information
        FrogMating::create($request->all());
        Session::flash('alert-success', 'Successfully set Mating B/W Frog.');
        return redirect()->route('mating.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param FrogMating $frogMating
     */
    public function edit($id)
    {
        // render edit form
        $frogMatting = FrogMating::find($id);
        if($frogMatting->status !== '2'){
            return view('mating.edit',compact('frogMatting'));
        } else {
            Session::flash('alert-warning', 'Frog Mating process already Completed.');
            return redirect()->route('mating.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FrogMatingRequest|Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param FrogMating $frogMating
     */
    public function update(FrogMatingRequest $request, $id)
    {
        // update matting status
        $frogMating = FrogMating::find($id);
        $frogMating->status = $request->status;
        $frogMating->save();
        Session::flash('alert-success', 'Frog Mating Status updated successfully.');
        return redirect()->route('mating.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param FrogMating $frogMating
     */
    public function destroy($id)
    {
        // check logged User is admin or not.
        if (Auth::user()->is_admin) {
            // frog soft delete operation perform
            $frogMating = FrogMating::find($id);
            $frogMating->is_deleted = "1";
            $frogMating->save();

            Session::flash('alert-success', 'Frog Mating information deleted successfully.');
            return redirect()->route('mating.index');
        } else {
            Session::flash('alert-danger', 'User not allow to perform this operation.');
            return redirect()->route('mating.index');
        }
    }
}
