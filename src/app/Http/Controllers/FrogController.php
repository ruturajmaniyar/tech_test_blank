<?php

namespace App\Http\Controllers;

use App\Frog;
use App\Http\Requests\FrogRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class FrogController extends Controller
{

    /**
     * create new controller Instance
     *
     * check user is authorize or not
     *
     * FrogController constructor.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param FrogRequest|Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(FrogRequest $request)
    {
        // get all Frog collection
        $frogs = Frog::where(['is_deleted' => '0'])->orderBy('id', 'ASC')->paginate(5);
        return view('frog.index', compact('frogs'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // render create form
        return view('frog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FrogRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FrogRequest $request)
    {
        // Add new Frog information
        Frog::create($request->all());
        Session::flash('alert-success', 'Successfully new Frog entry in pond.');
        return redirect()->route('frog.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param frog $frog
     */
    public function edit($id)
    {
        // render edit form
        $frog = Frog::find($id);
        return view('frog.edit',compact('frog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FrogRequest|Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param frog $frog
     */
    public function update(FrogRequest $request, $id)
    {
        // Update information about Frog
        if($request->has('is_death') && $request->get('is_death') == "0"){
            $request->merge(['death_date'=>null]);
        }
        Frog::find($id)->update($request->all());
        Session::flash('alert-success', 'Frog information updated successfully.');
        return redirect()->route('frog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param frog $frog
     */
    public function destroy($id)
    {
        // check logged User is admin or not.
        if (Auth::user()->is_admin) {
            // frog soft delete operation perform
            $frog = Frog::find($id);
            $frog->is_deleted = "1";
            $frog->save();

            Session::flash('alert-success', 'Frog information deleted successfully.');
            return redirect()->route('frog.index');
        } else {
            Session::flash('alert-danger', 'User not allow to perform this operation.');
            return redirect()->route('frog.index');
        }
    }
}
